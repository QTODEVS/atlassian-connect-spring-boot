package com.atlassian.connect.spring.internal.descriptor;

import com.atlassian.connect.spring.IgnoreJwt;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

/**
 * A controller that serves the Atlassian Connect add-on descriptor (<code>atlassian-connect.json</code>).
 */
@IgnoreJwt
@Controller
public class AddonDescriptorController {

    private static final Logger log = LoggerFactory.getLogger(AddonDescriptorController.class);

    static final String DESCRIPTOR_REQUEST_PATH = "/" + AddonDescriptorLoader.DESCRIPTOR_FILENAME;

    @Autowired
    private AddonDescriptorLoader addonDescriptorLoader;

    @RequestMapping(value = DESCRIPTOR_REQUEST_PATH, method = GET, produces = "application/json")
    @ResponseBody
    public String getDescriptor() throws IOException {
        String rawDescriptor = addonDescriptorLoader.getRawDescriptor();
        log.debug("Serving add-on descriptor: {}", rawDescriptor);
        return rawDescriptor;
    }
}
