package com.atlassian.connect.spring.it.util;

import com.atlassian.connect.spring.AtlassianHost;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.client.ClientHttpRequestInterceptor;

import java.util.Optional;

public class SimpleJwtSigningRestTemplate extends TestRestTemplate {

    public SimpleJwtSigningRestTemplate(String jwt) {
        this(new FixedJwtSigningClientHttpRequestInterceptor(jwt));
    }

    public SimpleJwtSigningRestTemplate(AtlassianHost host, Optional<String> optionalSubject) {
        this(new AtlassianHostJwtSigningClientHttpRequestInterceptor(host, optionalSubject));
    }

    public SimpleJwtSigningRestTemplate(ClientHttpRequestInterceptor requestInterceptor) {
        super(new RestTemplateBuilder().additionalInterceptors(requestInterceptor));
    }
}
