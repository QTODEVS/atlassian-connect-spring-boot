package com.atlassian.connect.spring.it.descriptor;

import ch.qos.logback.classic.Level;
import com.atlassian.connect.spring.internal.descriptor.AddonDescriptorLoader;
import com.atlassian.connect.spring.it.util.BaseApplicationIT;
import com.atlassian.connect.spring.it.util.LogbackCapture;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static com.atlassian.connect.spring.it.util.LogbackEventMatcher.logbackEvent;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AddonDescriptorLoaderUnresolvedPropertyIT extends BaseApplicationIT {

    @Rule
    public LogbackCapture logbackCapture = new LogbackCapture();

    @Autowired
    private AddonDescriptorLoader addonDescriptorLoader;

    @Test
    public void shouldWarnOnUnresolvedPlaceholder() {
        addonDescriptorLoader.getRawDescriptor();
        String message = "Add-on descriptor contains unresolved configuration placeholder: Could not resolve placeholder 'add-on.undefined-port'";
        assertThat(logbackCapture.getEvents(), hasItem(logbackEvent(is(Level.WARN), containsString(message))));
    }
}
