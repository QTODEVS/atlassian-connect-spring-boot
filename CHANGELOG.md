# Changelog

All notable changes to this project will be documented in this file.

## 1.4.0 - 2018-02-05

- [ACSPRING-75](https://ecosystem.atlassian.net/browse/ACSPRING-75) Upgrade to nimbus-jose-jwt 4.x

## 1.3.6 - 2018-01-31

- [ACSPRING-65](https://ecosystem.atlassian.net/browse/ACSPRING-65) Improve reporting for start-up failure due to missing AtlassianHostRepository bean
- [ACSPRING-66](https://ecosystem.atlassian.net/browse/ACSPRING-66) Miscellaneous archetype improvements
- [ACSPRING-68](https://ecosystem.atlassian.net/browse/ACSPRING-68) Improve logging for subsequent installation request
- [ACSPRING-73](https://ecosystem.atlassian.net/browse/ACSPRING-73) Asynchronous controller methods return Unauthorized

## 1.3.5 - 2017-07-31

- [ACSPRING-62](https://ecosystem.atlassian.net/browse/ACSPRING-62) Add index for column base_url in table atlassian_host

## 1.3.4 - 2017-07-17

- [ACSPRING-61](https://ecosystem.atlassian.net/browse/ACSPRING-61) Improve log message for JWT signature mismatch

## 1.3.3 - 2017-06-22

- [ACSPRING-60](https://ecosystem.atlassian.net/browse/ACSPRING-60) Allow disabling the redirection from / to /atlassian-connect.json

## 1.3.2 - 2017-05-23

- [ACSPRING-55](https://ecosystem.atlassian.net/browse/ACSPRING-55) Installation details are stored even when the Atlassian host closes the connection due to a timeout
- [ACSPRING-57](https://ecosystem.atlassian.net/browse/ACSPRING-57) Use saved host when firing application event for installation
- [ACSPRING-58](https://ecosystem.atlassian.net/browse/ACSPRING-58) Upgrade to Spring Boot 1.5.3

## 1.3.1 - 2017-02-17

- [ACSPRING-50](https://ecosystem.atlassian.net/browse/ACSPRING-50) Enable use of RestTemplateCustomizers
- [ACSPRING-51](https://ecosystem.atlassian.net/browse/ACSPRING-51) Improve console logging
- [ACSPRING-53](https://ecosystem.atlassian.net/browse/ACSPRING-53) Pluses in query parameters are not decoded in canonical request calculation for outbound requests

## 1.3.0 - 2017-01-23

- [ACSPRING-30](https://ecosystem.atlassian.net/browse/ACSPRING-30) AddonDescriptorController does not honor Spring Boot's contextPath
- [ACSPRING-31](https://ecosystem.atlassian.net/browse/ACSPRING-31) atlassian-connect-* model attributes cannot be used with Thymeleaf
- [ACSPRING-42](https://ecosystem.atlassian.net/browse/ACSPRING-42) Error when using injected RestTemplate with an HttpEntity containing an Authorization header
- [ACSPRING-45](https://ecosystem.atlassian.net/browse/ACSPRING-45) RestTemplate sets JWT subject claim
- [ACSPRING-46](https://ecosystem.atlassian.net/browse/ACSPRING-46) Obtain JWT for making request to given host URL
- [ACSPRING-47](https://ecosystem.atlassian.net/browse/ACSPRING-47) Send User-Agent header when using OAuth 2.0
- [ACSPRING-49](https://ecosystem.atlassian.net/browse/ACSPRING-49) Obtain OAuth 2.0 access token for making request to host

## 1.2.1 - 2017-01-05

- [ACSPRING-43](https://ecosystem.atlassian.net/browse/ACSPRING-43) Use the principal of the Authentication object to determine successful authentication

## 1.2.0 - 2016-12-23

- [ACSPRING-8](https://ecosystem.atlassian.net/browse/ACSPRING-8) Support authenticating requests from iframe content back to the add-on
- [ACSPRING-24](https://ecosystem.atlassian.net/browse/ACSPRING-24) Accept JWT self-authentication tokens
- [ACSPRING-33](https://ecosystem.atlassian.net/browse/ACSPRING-33) Query parameters are double-encoded in canonical request calculation for outbound requests
- [ACSPRING-36](https://ecosystem.atlassian.net/browse/ACSPRING-36) Configure the order of the JWT authentication filter
- [ACSPRING-37](https://ecosystem.atlassian.net/browse/ACSPRING-37) Enable configuration of JWT expiration time
- [ACSPRING-39](https://ecosystem.atlassian.net/browse/ACSPRING-39) Upgrade to Spring Boot 1.4.2
- [ACSPRING-40](https://ecosystem.atlassian.net/browse/ACSPRING-40) Miscellaneous improvements to integration tests
- [ACSPRING-41](https://ecosystem.atlassian.net/browse/ACSPRING-41) Miscellaneous archetype improvements

## 1.1.0 - 2016-11-01

- [ACSPRING-18](https://ecosystem.atlassian.net/browse/ACSPRING-18) Create integration tests using Spring Data MongoDB
- [ACSPRING-22](https://ecosystem.atlassian.net/browse/ACSPRING-22) Support OAuth 2 authentication with JWT client credentials
- [ACSPRING-29](https://ecosystem.atlassian.net/browse/ACSPRING-29) Include spring-boot:repackage in build for archetype add-on

## 1.0.0 - 2016-07-29

- [ACSPRING-7](https://ecosystem.atlassian.net/browse/ACSPRING-7) Fire application events on completion of lifecycle callbacks
- [ACSPRING-25](https://ecosystem.atlassian.net/browse/ACSPRING-25) Add-on uninstalled endpoint returns 400

## 1.0.0-beta-3 - 2016-07-18

- [ACSPRING-17](https://ecosystem.atlassian.net/browse/ACSPRING-17) Steps for modifying an existing spring-boot project are insufficient
- [ACSPRING-19](https://ecosystem.atlassian.net/browse/ACSPRING-19) JWT token header gets added multiple times when using RestTemplate and custom interceptors
